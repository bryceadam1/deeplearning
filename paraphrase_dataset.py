import torch

class ParaphraseDataset(torch.utils.data.Dataset):
    def __init__(self, filename):
        super(ParaphraseDataset, self).__init__()
        self.data = []
        with open(filename, "r") as file:
            while (line := file.readline()) != "":
                s1, s2, paraphrase = line.split("\t")
                paraphrase = int(paraphrase.strip())

                self.data.append((s1, s2, 2*paraphrase - 1))
    
    def __getitem__(self, i):
        return self.data[i]
    
    def __len__(self):
        return len(self.data)