import torch
from torch import nn as nn

from embedding import WordEmbedding
from residual_self_attention import ResidualSelfAttention
from residual_cross_attention import ResidualCrossAttention

class ParaphraseDetector(nn.Module):
    def __init__(self, embedding_file, vect_dim, num_heads = 8, num_comparisons = 6, num_feed_forward=2):
        super(ParaphraseDetector, self).__init__()

        self.embedding = WordEmbedding(embedding_file, vect_dim)
        self.vect_dim = vect_dim
        self.num_comparisons = num_comparisons
        self.num_feed_forward = num_feed_forward

        self.self_attention = nn.ModuleList([ResidualSelfAttention(num_heads, vect_dim) for _ in range(num_comparisons)])
        self.cross_attention = nn.ModuleList([ResidualCrossAttention(num_heads, vect_dim) for _ in range(num_comparisons)])
        self.feed_forward = nn.ModuleList([nn.Linear(vect_dim, vect_dim) for _ in range(num_feed_forward)])
        self.relu = nn.ReLU()

    def forward(self, phrase1, phrase2):
        x1 = self.embedding(phrase1)
        x2 = self.embedding(phrase2)

        for i in range(self.num_comparisons):
            x1 = self.self_attention[i](x1)
            x2 = self.self_attention[i](x2)

            x1, x2 = self.cross_attention[i](x1, x2, x2), self.cross_attention[i](x2, x1, x1)
        
        for i in range(self.num_feed_forward):
            x1 = x1 + self.relu(self.feed_forward[i](x1))
            x2 = x2 + self.relu(self.feed_forward[i](x2))
        
        x1 = torch.sum(x1, dim=-2)/x1.shape[-2]
        x2 = torch.sum(x2, dim=-2)/x2.shape[-2]

        return x1, x2
