import torch
from torch import nn as nn
from torch.nn import functional as F

class MultiHeadAttention(nn.Module):
    def __init__(self, num_heads, in_dim, out_dim = None, proj_dim = None):
        super(MultiHeadAttention, self).__init__()

        self.num_heads = num_heads
        self.in_dim = in_dim
        self.out_dim = out_dim or in_dim
        self.proj_dim = proj_dim or in_dim

        self.W_Q = nn.ModuleList([nn.Linear(self.in_dim, self.proj_dim) for _ in range(num_heads)])
        self.W_K = nn.ModuleList([nn.Linear(self.in_dim, self.proj_dim) for _ in range(num_heads)])
        self.W_V = nn.ModuleList([nn.Linear(self.in_dim, self.proj_dim) for _ in range(num_heads)])

        self.W_O = nn.Linear(num_heads*self.proj_dim, self.out_dim)
    
    def attention(self, Q, K, V):
        return torch.matmul(F.softmax(torch.matmul(Q,torch.transpose(K, -2, -1))/self.proj_dim, dim=-1), V)
    
    def forward(self, Q, K, V):
        out = torch.cat([self.attention(self.W_Q[i](Q), self.W_K[i](K), self.W_V[i](V)) for i in range(self.num_heads)], dim=-1)
        return self.W_O(out)
