import torch
from torch import nn

from multi_head_attention import MultiHeadAttention

class ResidualCrossAttention(nn.Module):
    def __init__(self, num_heads, in_dim):
        super(ResidualCrossAttention, self).__init__()

        self.self_attention = MultiHeadAttention(num_heads, in_dim)
        self.cross_attention = MultiHeadAttention(num_heads, in_dim)
        self.feed_forward = nn.Linear(in_dim, in_dim)
        self.relu = nn.ReLU()
        self.layer_norm = nn.LayerNorm(in_dim)
    
    def forward(self, x, K, V):
        x = x + self.self_attention(x, x, x)
        x = self.layer_norm(x)
        x = x + self.cross_attention(x, K, V)
        x = self.layer_norm(x)
        x = x + self.relu(self.feed_forward(x))
        x = self.layer_norm(x)
        return x