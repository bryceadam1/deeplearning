import torch
import numpy as np
import spacy
# python -m spacy download en

class WordEmbedding(torch.nn.Module):
    def __init__(self, filename, vect_dim):
        super(WordEmbedding, self).__init__()
        self.vect_dim = vect_dim
        self.spacy_en = spacy.load('en')
        self.build_word_dict(filename, vect_dim)

        # It is possible that we will come across a word we don't know yet.
        # Several ideas for how to handle these cases include:
        #   1. Removing it from the sentence and pretending it never existed
        #   2. Creating a word embedding for 'unknown words'
        #   3. Pass the letters of the token into a convolutional network and learn the meaning of the word.
        # I have opted for a combination of the last two methods, to simply learn an unknown token, since
        # I expect there to not be too many unknown tokens, as my input will come from a speech-to-text engine.
        self.unknown_token = torch.nn.Parameter(torch.from_numpy(np.random.uniform(low=-1, high=1, size=vect_dim)).float())
    
    def build_word_dict(self, filename, vect_dim):
        self.word_dict = {}
        with open(filename) as file:
            while (line := file.readline()) != "":
                line_args = line.strip().split(" ")
                vect = list(map(float, line_args[-vect_dim:]))
                word = " ".join(line_args[:-vect_dim])
                self.word_dict[word] = torch.tensor(vect, requires_grad=False).float()
    
    def get_embedding(self, word):
        try:
            if self.unknown_token.device.type == 'cuda':
                return self.word_dict[word].cuda()
            else:
                return self.word_dict[word]
        except Exception as e:
            # If the word has no pretrained embedding, we will assign one, and attempt to learn the proper embedding
            return self.unknown_token
    
    def forward(self, sentence):
        return torch.stack([self.get_embedding(str(word)) for word in self.spacy_en.tokenizer(sentence)], dim=0)
