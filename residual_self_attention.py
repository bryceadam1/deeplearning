import torch
from torch import nn as nn

from multi_head_attention import MultiHeadAttention


class ResidualSelfAttention(nn.Module):
    def __init__(self, num_heads, in_dim):
        super(ResidualSelfAttention, self).__init__()

        self.multi_head_attention = MultiHeadAttention(num_heads, in_dim)
        self.feed_forward = nn.Linear(in_dim, in_dim)
        self.relu = nn.ReLU()
        self.layer_norm = nn.LayerNorm(in_dim)
    
    def forward(self, x):
        x = x + self.multi_head_attention(x, x, x)
        x = self.layer_norm(x)
        x = x + self.relu(self.feed_forward(x))
        x = self.layer_norm(x)
        return x
        