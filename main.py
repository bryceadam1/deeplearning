from train_paraphrase import train

def main():
    train_progress_file = open("train_progress.txt","a")
    test_progress_file = open("test_progress.txt","a")
    try:
        train("data/clean/msr_paraphrase_train.txt", "data/clean/msr_paraphrase_test.txt", "glove.840B.300d.txt", 300, 1, val_every=1, train_progress_file=train_progress_file, test_progress_file=test_progress_file, save_model="paraphrase_detector.pt", save_every=1)
    finally:
        train_progress_file.close()
        test_progress_file.close()

if __name__=="__main__":
    main()