import torch
import torch.nn as nn
from torch.optim import Adam
from torch.utils.data import DataLoader
from tqdm import tqdm

from paraphrase_dataset import ParaphraseDataset
from paraphrase_detector import ParaphraseDetector

def train(train_filename, test_filename, embedding_filename, vect_dim, epochs, val_every = 1, train_progress_file = None, test_progress_file = None, load_model = None, save_model = None, save_every = 1):
    train_dataset = ParaphraseDataset(train_filename)
    test_dataset = ParaphraseDataset(test_filename)
    train_loader = DataLoader(train_dataset)
    test_dataset = DataLoader(test_dataset)

    if load_model is not None:
        saved_dict = torch.load(load_model)

    model = ParaphraseDetector(embedding_filename, vect_dim)
    if load_model is not None:
        model.load_state_dict(saved_dict['model_state_dict'])
    if torch.cuda.is_available():
        model = model.cuda()
        model.train()
    objective = nn.CosineEmbeddingLoss()
    optimizer = Adam(model.parameters(), lr=1e-4)
    if load_model is not None:
        optimizer.load_state_dict(saved_dict['optimizer_saved_dict'])
    if torch.cuda.is_available():
        optimizer = optimizer.cuda()
    optimizer.zero_grad()

    training_loss = []
    test_loss = []

    for epoch in range(epochs):
        print(f"Epoch: {epoch}")
        for s1, s2, y in tqdm(train_loader):
            if torch.cuda.is_available():
                y = y.cuda()
            x1, x2 = model(s1[0], s2[0])
            loss = objective(torch.unsqueeze(x1,dim=0), torch.unsqueeze(x2,dim=0), y)
            
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

            training_loss.append(loss.item())
        
        if train_progress_file is not None:
            for val in training_loss:
                train_progress_file.write(f"{val}\n")
            training_loss = []
        
        if epoch % val_every == 0:
            with torch.no_grad:
                model.eval()
                for s1, s2, y in tqdm(test_loader):
                    if torch.cuda.is_available():
                        y = y.cuda()
                    x1, x2 = model(s1[0], s2[0])
                    loss = objective(torch.unsqueeze(x1,dim=0), torch.unsqueeze(x2,dim=0), y)

                    test_loss.append(loss.item())
                model.train()

            if test_progress_file is not None:
                for val in test_loss:
                    test_progress_file.write(f"{val}\n")
                test_loss = []

        if save_model is not None and epoch % save_every == 0:
            torch.save(
                {
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict()
                }, save_model
            )
    
    if save_model is not None:
        torch.save(
            {
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()
            }, save_model
        )
    